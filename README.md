# Advanced Git Workshop
Lab 15: Working with subtreess

---

## Preparations

 - Let's initialize our environment:
 
```
$ mkdir ~/lab15
$ cd ~/lab15
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git subtree-repo
```

---

## Working with subtrees

- Add a nested-repo as a subtree to the repository:
```
$ cd ~/lab15/subtree-repo
$ git subtree --prefix=subtree add https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/nested-repo.git master
```

  - Create a file in the main project:
```
$ echo content > newfile.txt
```

  - Create a file within the subtree:
```
$ cd subtree
$ echo content > newfile.txt
```

  - Check the status from the main project:
```
$ cd ..
$ git status
```

 - Note that you manage the subtree as a part of your repository

 - Commit the changes: 
```
$ git add -A
$ git commit -m "main project and subtree changes"
```

 - Let's inspect the project history: 
```
$ git log --oneline --decorate
```

 - As you can see the subtree repository was merged to the main project history
 
 - If you want to update the subtree reference you can do it using the command: 
```
$  git subtree pull --prefix=subtree https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/nested-repo.git feature
```
 
 - Note that the history will be merged (sometimes causing conflicts)
 